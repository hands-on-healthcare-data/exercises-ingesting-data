# Ingesting Synthea Data

This repository contains examples for ingesting data into different databases.

These examples were created as part of an Applied Data Engineering course
specialization. Video walkthroughs are currently in production (as of Summer
2022). Since this repo will be used as part of course, they are structured more
like exercises rather than complete projects.

The exercises can also be used to apply the principles covered in my book:
[Hands-On Healthcare Data](https://www.oreilly.com/library/view/hands-on-healthcare-data/9781098112912/).

## Data

This repo uses a set of data generated using the
[Synthea](https://github.com/synthetichealth/synthea) library, v3.0.0

## Usage

Each of the supported databases has a `docker-compose.yml` file such that
calling `docker compose up` in the corresponding directory should get things
going. Unless otherwise specified, shared volumes are used to prevent data loss
with the corresponding directory created in $PWD

## Supported Databases

### PostgreSQL

The provided `docker-compose.yml` file will instantiate:

1. PostgreSQL 14.1.x

   Default username/password: postgres/password

2. pgAdmin 4

   Default username/password: postgres@pgadmin.org/admin


### Neo4j

The provided `docker-compose.yml` file will instantiate a single container
running Neo4j 4.4 Community Edition.  Additional plugins are enabled and the heap size is also increased.  

Default username/password: neo4j/test

## License

Copyright © 2022 Andrew Nguyen

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
